﻿// ConsoleApplication2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "../Geometry/Geometry.h"

using namespace std;

int main()
{
	//Point p1(1, 1, 0); // is Parallel
	//Point p2(2, 2, 0);
	//Point p3(1, 2, 0);
	//Point p4(2, 3, 0);

	Point p1(1, 1, 0);
	Point p2(2, 2, 0);
	Point p3(1, 1, 1);
	Point p4(1, 1, 2);

	Line l1(p1, p2);
	Line l2(p3, p4);

	Geometry g;
	
	cout << "line 1 : " << l1;
	cout << "line 2 : " << l2;
	//cout << "line 1 :";
	//l1.PrintCanonicalEquation();

	//cout << "line 2 :";
	//l2.PrintCanonicalEquation();

	cout << "parallelism check line l1 and l2 : ";

	if (g.IsParallel(l1, l2)) {
		cout << "yes" << endl;;
	}
	else {
		cout << "no" << endl;;
	}
	 
	cout << "perpendicular check line l1 and l2 : ";

	if (g.IsPerpendicular(l1, l2)) {
		cout << "yes" << endl;;
	}
	else {
		cout << "no" << endl;;
	}

	Point p = g.FindIntersection(l1, l2);
	cout << "find intersection line l1 and l2 : x - " << p.GetX() << " y - " << p.GetY() << " z - " << p.GetZ() << endl;

	double distanse = g.FindDistance(l1, l2);
	cout << "find distanse between line l1 and l2 : " << distanse << endl;

	Plane pl(p1, p2, p3);
	cout << "plane : " << pl;
}
