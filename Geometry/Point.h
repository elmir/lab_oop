#pragma once
class Point
{
private:
	double x;
	double y;
	double z;

public:
	Point();
	Point(double x, double y, double z);
	double GetX() const;
	double GetY() const;
	double GetZ() const;
	void SetX(double x);
	void SetY(double y);
	void SetZ(double z);
};

