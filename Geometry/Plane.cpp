#include "Plane.h"
#include <ostream>
#include <string>

Plane::Plane(Point p1, Point p2, Point p3) {
	this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
}

Point Plane::GetP1() const {
	return p1;
}

Point Plane::GetP2() const {
	return p2;
}

Point Plane::GetP3() const {
	return p2;
}

void Plane::FindNormalVector(double& i, double& j, double& k) const {
	i = ((p2.GetY() - p1.GetY()) * (p3.GetZ() - p1.GetZ()) - (p3.GetY() - p1.GetY()) * (p3.GetZ() - p1.GetZ()));
	j = -((p2.GetX() - p1.GetX()) * (p3.GetZ() - p1.GetZ()) + (p3.GetX() - p1.GetX()) * (p3.GetZ() - p1.GetZ()));
	k = ((p2.GetX() - p1.GetX()) * (p3.GetZ() - p1.GetZ()) - (p3.GetX() - p1.GetX()) * (p2.GetY() - p1.GetY()));
	return;
}

void Plane::GetCanonicalEquation(double& A, double& B, double& C, double& D) const {
	double i, j, k;
	FindNormalVector(i, j, k);
	A = i;
	B = j;
	C = k;
	D = i * (-p1.GetX()) + j * (-p1.GetY()) + k * (-p1.GetZ());
}


std::ostream& operator<<(std::ostream& os, const Plane& pl)
{
	double A, B, C, D;
	pl.GetCanonicalEquation(A, B, C, D);
	os << std::to_string(A) << "x";
	if (B > 0) {
		os << " + " << std::to_string(B) << "y";
	}
	else if (B < 0) {
		os << " - "<< std::to_string(-B) << "y";
	}
	if (C > 0) {
		os << " + " << std::to_string(C) << "z";
	}
	else if (C < 0) {
		os << " - " << std::to_string(-C) << "z";
	}
	if (D > 0) {
		os << " + " << std::to_string(D);
	}
	else if (C < 0) {
		os << " - " << std::to_string(-D);
	}
	os << " = 0";
	return os;
}