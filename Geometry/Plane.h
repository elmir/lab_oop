#pragma once
#include "Point.h"
#include <ostream>


class Plane
{
private:
	Point p1;
	Point p2;
	Point p3;
public:
	Plane(Point p1, Point p2, Point p3);
	Point GetP1() const;
	Point GetP2() const;
	Point GetP3() const;
	void GetCanonicalEquation( double &A, double &B, double& C, double& D) const;
	void FindNormalVector(double& x, double& y, double& z) const;
};

std::ostream& operator<<(std::ostream& os, const Plane& l);
