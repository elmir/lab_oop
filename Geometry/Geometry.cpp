// Geometry.cpp : Defines the functions for the static library.
//

#include "pch.h"
#include "framework.h"


#include "Geometry.h"
#include <algorithm>
#include <math.h>

#define INF 100000007

bool Geometry::IsParallel(Line l1, Line l2) {

	double aX, aY, aZ;
	double bX, bY, bZ;

	//find vectors a � b for lines l1 � l2
	l1.GetDirectionVector(aX, aY, aZ);
	l2.GetDirectionVector(bX, bY, bZ);

	// vector product of vectors a � b
	double x = aY * bZ - aZ * bY;
	double y = aZ * bX - aX * bZ;
	double z = aX * bY - aY * bX;

	if (x == 0 && y == 0 && z == 0) {
		return true;
	}
	return false;
}
bool Geometry::IsPerpendicular(Line l1, Line l2) {
	double aX, aY, aZ;
	double bX, bY, bZ;

	//find vectors a � b for lines l1 � l2
	l1.GetDirectionVector(aX, aY, aZ);
	l2.GetDirectionVector(bX, bY, bZ);

	if ((aX * bX + aY * bY + aZ * bZ) == 0) {
		return true;
	}
	return false;
}

Point Geometry::FindIntersection(Line l1, Line l2) {
	double aX, aY, aZ;
	double bX, bY, bZ;
	double cX, cY, cZ;

	//find vectors a � b for lines l1 � l2
	l1.GetDirectionVector(aX, aY, aZ);
	l2.GetDirectionVector(bX, bY, bZ);

	//find vector between point l1.p1 � l2.p1
	FindVectorBetweenPoints(l1.GetP1(), l2.GetP2(), cX, cY, cZ);

	//build a matrix to check the coplanarity of the vectors (are three vectors in the same plane)
	matrix m = { {aX, bX, cX}, {aY, bY, cY}, {aZ, bZ, cZ} };

	double det = Det(m);

	if (det == 0 && !IsParallel(l1, l2)) {
		// solve the system for finding the intersection point
		matrix M = { {aX, -bX, l1.GetP1().GetX() - l2.GetP1().GetX()},
					 {aY, -bY, l1.GetP1().GetY() - l2.GetP1().GetY()},
					 {aZ, -bZ, l1.GetP1().GetZ() - l2.GetP1().GetZ()} };
		std::vector<double>answ;
		int res = Gauss(M, answ);
		Point p;
		p.SetX(answ[0] * aX + l1.GetP1().GetX());
		p.SetY(answ[0] * aY + l1.GetP1().GetY());
		p.SetZ(answ[0] * aZ + l1.GetP1().GetZ());
		return p;
	}
}

double Geometry::FindDistance(Line l1, Line l2) {
	double aX, aY, aZ;
	double bX, bY, bZ;
	double cX, cY, cZ;

	//find vectors a � b for lines l1 � l2
	l1.GetDirectionVector(aX, aY, aZ);
	l2.GetDirectionVector(bX, bY, bZ);

	double distanse = 0;

	if (IsParallel(l1, l2)) {
		double x = (l2.GetP1().GetX() - l1.GetP1().GetX()) * aY - (l2.GetP1().GetY() - l1.GetP1().GetY()) * aX;
		double y = (l2.GetP1().GetY() - l1.GetP1().GetY()) * aZ - (l2.GetP1().GetZ() - l1.GetP1().GetZ()) * aY;
		double z = (l2.GetP1().GetZ() - l1.GetP1().GetZ()) * aX - (l2.GetP1().GetX() - l1.GetP1().GetX()) * aZ;
		distanse = sqrt((x * x + y * y + z * z)) / sqrt(aX * aX + aY * aY + aZ * aZ);
	}
	else {

		//find vector between point l1.p1 � l2.p1
		FindVectorBetweenPoints(l1.GetP1(), l2.GetP2(), cX, cY, cZ);

		//build a matrix to check the coplanarity of the vectors (are three vectors in the same plane)
		matrix m = { {cX, cY, cZ}, {aX, aY, aZ}, {bX, bY, bZ} };
		double det = Det(m);

		//  vector product of vectors a � b
		double x = aY * bZ - aZ * bY;
		double y = aZ * bX - aX * bZ;
		double z = aX * bY - aY * bX;
		distanse = det / sqrt(x * x + y * y + z * z);
	}
	return distanse;
}

bool Geometry::IsBelongs(Point p, Line l) {
	//TODO: Implementation
	return false;
}
void Geometry::FindNormalVector(Line l, double& x, double& y, double& z) {
	//TODO: Implementation
	return;
}
bool Geometry::IsParallel(Plane pl1, Line pl2) {
	//TODO: Implementation
	return false;
}

bool Geometry::IsPerpendicular(Plane pl1, Line pl2) {
	//TODO: Implementation
	return false;
}
Point Geometry::FindIntersection(Plane pl1, Line pl2) {
	//TODO: Implementation
	Point p;
	return p;
}
double Geometry::FindDistance(Plane pl1, Line pl2) {
	//TODO: Implementation
	return 0;
}

bool Geometry::IsBelongs(Point p, Plane pl) {
	//TODO: Implementation
	return 0;
}

bool Geometry::IsBelongs(Line l, Plane pl) {
	//TODO: Implementation
	return 0;
}

void Geometry::FindVectorBetweenPoints(Point p1, Point p2, double& x, double& y, double& z) {
	x = p2.GetX() - p1.GetX();
	y = p2.GetY() - p1.GetY();
	z = p2.GetZ() - p1.GetZ();
}

double Geometry::Det(matrix m) {
	double det = 1;
	for (int i = 0; i < m.size(); ++i) {
		int k = i;
		for (int j = i + 1; j < m.size(); ++j)
			if (abs(m[j][i]) > abs(m[k][i]))
				k = j;
		if (abs(m[k][i]) < 1E-9) {
			det = 0;
			break;
		}
		swap(m[i], m[k]);
		if (i != k)
			det = -det;
		det *= m[i][i];
		for (int j = i + 1; j < m.size(); ++j)
			m[i][j] /= m[i][i];
		for (int j = 0; j < m.size(); ++j)
			if (j != i && abs(m[j][i]) > 1E-9)
				for (int k = i + 1; k < m.size(); ++k)
					m[j][k] -= m[i][k] * m[j][i];
	}
	return det;

}

int Geometry::Gauss(matrix a, std::vector<double>& ans) {
	int n = (int)a.size();
	int m = (int)a[0].size() - 1;

	std::vector<int> where(m, -1);
	for (int col = 0, row = 0; col < m && row < n; ++col) {
		int sel = row;
		for (int i = row; i < n; ++i)
			if (abs(a[i][col]) > abs(a[sel][col]))
				sel = i;
		if (abs(a[sel][col]) < 1E-9)
			continue;
		for (int i = col; i <= m; ++i)
			std::swap(a[sel][i], a[row][i]);
		where[col] = row;

		for (int i = 0; i < n; ++i)
			if (i != row) {
				double c = a[i][col] / a[row][col];
				for (int j = col; j <= m; ++j)
					a[i][j] -= a[row][j] * c;
			}
		++row;
	}

	ans.assign(m, 0);
	for (int i = 0; i < m; ++i)
		if (where[i] != -1)
			ans[i] = a[where[i]][m] / a[where[i]][i];
	for (int i = 0; i < n; ++i) {
		double sum = 0;
		for (int j = 0; j < m; ++j)
			sum += ans[j] * a[i][j];
		if (abs(sum - a[i][m]) > 1E-9)
			return 0;
	}

	for (int i = 0; i < m; ++i)
		if (where[i] == -1)
			return INF;
	return 1;
}

