#pragma once
#include "Line.h"
#include "Plane.h"
#include <vector>


typedef std::vector<std::vector<double>> matrix ;

class Geometry
{
public:
	bool IsParallel(Line l1, Line l2);
	bool IsPerpendicular(Line l1, Line l2);
	Point FindIntersection(Line l1, Line l2);
	double FindDistance(Line l1, Line l2);
	bool IsBelongs(Point p, Line l);
	void FindNormalVector(Line l, double &x, double &y, double &z);

	bool IsParallel(Plane pl1, Line pl2);
	bool IsPerpendicular(Plane pl1, Line pl2);
	Point FindIntersection(Plane pl1, Line pl2);
	double FindDistance(Plane pl1, Line pl2);
	bool IsBelongs(Point p, Plane pl);
	bool IsBelongs(Line l, Plane pl);
	
private:
	void FindVectorBetweenPoints(Point p1, Point p2, double& x, double& y, double& z);
	double Det(matrix m);
	int Gauss(matrix a, std::vector<double>& ans);
};

