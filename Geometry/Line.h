#pragma once
#include "Point.h"
#include <string>
#include <ostream>

class Line
{
private: 
	Point p1;
	Point p2;
public:
	Line(Point p1, Point p2);
	Point GetP1() const;
	Point GetP2() const;
	void GetDirectionVector(double &x, double &y, double &z) const;
	//void PrintCanonicalEquation();
};

std::ostream& operator<<(std::ostream& os, const Line& l);
