#include "Line.h"
#include <iostream>

Line::Line(Point p1, Point p2) {
	this->p1 = p1;
	this->p2 = p2;
}

Point Line::GetP1() const{
	return p1;
}

Point Line::GetP2() const {
	return p2;
}

void Line::GetDirectionVector(double& x, double& y, double& z) const{
	x = p1.GetX() - p2.GetX();
	y = p1.GetY() - p2.GetY();
	z = p1.GetZ() - p2.GetZ();
}

//void Line::PrintCanonicalEquation() {
//	double aX, aY, aZ;
//	GetDirectionVector(aX, aY, aZ);
//	std::cout << "(x - " << p1.GetX() << ")/" << aX << " = (y - " << p1.GetY() << ")/" << aY << " = (z - " << p1.GetZ() << ")/" << aZ << std::endl;
//}

std::ostream& operator<<(std::ostream& os, const Line& l)
{
	double aX, aY, aZ;
	l.GetDirectionVector(aX, aY, aZ);
	os << "(x - " + std::to_string(l.GetP1().GetX()) + ")/" + std::to_string(aX) + " = (y - " + std::to_string(l.GetP1().GetY()) + ")/" + std::to_string(aY)
		+ " = (z - " + std::to_string(l.GetP1().GetZ()) + ")/" + std::to_string(aZ) << std::endl;
	return os;
}