#include "Point.h"

Point::Point() {
	x = 0;
	y = 0;
	z = 0;
}
Point::Point(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
double Point::GetX() const {
	return x;
}
double Point::GetY() const{
	return y;
}
double Point::GetZ() const {
	return z;
}
void Point::SetX(double x) {
	this->x = x;
}
void Point::SetY(double y) {
	this->y = y;
}
void Point::SetZ(double z) {
	this->z = z;
}